package br.com.neppo;

public class MathUtil {

    /**
     * Dado um conjunto de n�meros inteiros "ints" e um n�mero arbitr�rio "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */
    public static boolean subsetSumChecker(int ints[], int sum) throws Exception {

        if (ints == null) {
            throw new IllegalArgumentException("Conjunto nao pode ser nulo!");
        }

        if (sum == 0)
            return true;

        int total, index;

        for (int i = 0; i < ints.length; i++) {
            index = i;
            total = 0;
            for (int j = i; j < ints.length; j++) {
                total += ints[j];
                if (total == sum) {
                    return true;
                }
            }
        }
        // throw new UnsupportedOperationException("Please implement me.");
        return false;
    }
}

